<?php

namespace App\Model;

use Nette\InvalidArgumentException;
use Tracy\Debugger;


class CompanyModel extends BaseModel
{
    /**
     * Metoda vrací seznam všech firem seřazené podle jména
     */
    public function listCompanies()
    {
        return $this->findAll()->order('name');
    }

    /**
     * Metoda vrací firmu se zadaným id, pokud neexistuje vrací NoDataFound.
     * @param int $id
     * @return static
     * @throws NoDataFound
     */
    public function getCompany($id)
    {
        return $this->findById($id);
    }

    /**
     * Metoda vrací vloží novou firmu
     * @param array $values
     * @return bool|int|\Nette\Database\Table\IRow $id vložené firmy
     */
    public function insertCompany($values)
    {
        if (!preg_match('/^([0-9]{3}(| )){3}$/', $values->phone) || !preg_match('/^[a-zA-z ]{1,30}$/', $values->name))
            throw new InvalidArgumentException();

        return $this->insert($values);
    }

    /**
     * Metoda edituje firmu, pokud neexistuje vrací NoDataFound.
     * @param array  $values
     */
    public function updateCompany($id, $values)
    {
        if (!preg_match('/^([0-9]{3}(| )){3}$/', $values->phone) || !preg_match('/^[a-zA-z ]{1,30}$/', $values->name))
            throw new InvalidArgumentException();

        return $this->update($id, $values);
    }

    /**
     * Metoda odebere firmu, pokud neexistuje vrací NoDataFound.
     * @param $id
     * @return
     * @internal param array $values
     */
    public function deleteCompany($id)
    {
        return $this->delete($id);
    }

    public function tableName()
    {
        return 'company';
    }
}