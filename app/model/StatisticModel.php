<?php

namespace App\Model;


class StatisticModel extends BaseModel
{

    /**
     * Metoda vrací seznam všech statistik firem, záznam bude mít položky název firmz, minální plat ve firmě, maximální plat ve firmě, průměrný plat a součet všech platů.
     */
    public function listStatistic()
    {
        $data = [];
        foreach ($this->getTable('company') as $company)
        {
            $emps = $this->getTable('employer')->where('company_id', $company->id);

            $sum = $emps->sum('salary');
            $min = $emps->min('salary');
            $max = $emps->max('salary');
            $count = $emps->count();

            if ($count == 0)
                $min = $max = $sum = 0;

            $data[$company->id] = [
                'name' => $company->name,
                'min' => $min,
                'max' => $max,
                'avg' => $count == 0 ? 0 : $sum/$count,
                'sum' => $sum
            ];
        }

        return $data;
    }

    public function tableName()
    {
        throw new NoDataFound('No table with statistics');
    }
}