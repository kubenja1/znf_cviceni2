<?php

namespace App\Model;

use Tracy\Debugger;


class EmployerModel extends BaseModel
{
    /**
     * Metoda vrací seznam všech zaměstanců řazené podle příjmení
     */
    public function listEmployers()
    {
        return $this->findAll();
    }

    /**
     * Metoda vrací zaměstnace se zadaným id, pokud neexistuje vrací NoDataFound.
     * @param int $id
     * @return \Nette\Database\Table\IRow
     */
    public function getEmployer($id)
    {
        return $this->findById($id);
    }

    /**
     * Metoda vrací vloží nového zaměstnance
     * @param array $values
     * @return bool|int|\Nette\Database\Table\IRow $id vloženého zaměstnance
     */
    public function insertEmployer($values)
    {
        return $this->insert($values);
    }

    /**
     * Metoda edituje zaměstance, pokud neexistuje vrací NoDataFound.
     * @param int  $id zamestnance
     * @param array  $values
     */
    public function updateEmployer($id, $values)
    {
        return $this->update($id, $values);
    }

    /**
     * Metoda odebere zaměstnance, pokud neexistuje vrací NoDataFound.
     * @param $id
     * @return
     * @internal param array $values
     */
    public function deleteEmployer($id)
    {
        return $this->delete($id);
    }

    public function tableName()
    {
        return 'employer';
    }
}