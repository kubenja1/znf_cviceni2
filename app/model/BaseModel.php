<?php

namespace App\Model;

use Nette;

class NoDataFound extends \Exception {};



abstract class BaseModel extends Nette\Object {

    protected $database;
/**
* @brief Konstruktor vytvarejici base model.
*/
    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;
    }

    protected function findAll()
    {
        return $this->getTable();
    }

    protected function findById($id)
    {
        $ret = $this->findAll()->get($id);
        if ($ret == NULL)
            throw new NoDataFound();
        return $ret;
    }

    protected function insert($data)
    {
        return $this->findAll()->insert($data);
    }

    protected function update($id, $data)
    {
        return $this->findById($id)->update($data);
    }

    protected function delete($id)
    {
        return $this->findById($id)->delete();
    }

    protected function getTable($name = NULL)
    {
        if ($name == NULL)
            $name = $this->tableName();
        return $this->database->table($name);
    }

    protected abstract function tableName();
}