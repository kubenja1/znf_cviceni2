<?php

namespace App\Presenters;

use App\Model\EmployerModel;
use App\Model\CompanyModel;
use App\Forms\EmployerFormFactory;
use Nette\Application\UI\Form;
use App\Model\NoDataFound;
use Tracy\Debugger;


/**
 * Should probably be EmployeePresenter
 *
 * Class EmployerPresenter
 * @package App\Presenters
 */
class EmployerPresenter extends BasePresenter
{
    /** @var EmployerFormFactory - Formulářová továrnička pro správu zaměstanců */
    private $formFactory;

    /**
     * Setter pro formulářovou továrničku a modely správy firem a zaměstanců
     * @param EmployerFormFactory|UserFormFactory $companyFormFactory automaticky injectovaná formulářová továrnička pro správu zaměstanců
     */
    public function injectEmployerDependencies(EmployerFormFactory $companyFormFactory)
    {
        $this->formFactory = $companyFormFactory;
    }

    /**
     * Akce pro vkádání
     */
    public function actionAdd() {
        $form = $this['addForm'];
    }

    /**
     * Akce pro editaci
     * @param int $id id zaměstnance
     */
    public function actionEdit($id) {
        $form = $this['editForm'];
        $form->setDefaults($this->_employerModel->getEmployer($id));
    }

    /**
     * Akce pro mazání
     * @param int $id id zaměstnance
     */
    public function actionDelete($id) {
        $form = $this['deleteForm'];
        $this->_employerModel->deleteEmployer($id);
    }

    /**
     * Metoda pro vytvoření formuláře pro vložení
     * @return Form - formulář
     */
    public function createComponentAddForm()
    {
        $form = $this->formFactory->createAddForm();
        $form->onSuccess[] = function (Form $form) {
            $this->redirect('Employer:default');
        };
        return $form;
    }

    /**
     * Metoda pro vytvoření formuáře pro editaci
     * @return Form - formulář
     */
    public function createComponentEditForm()
    {
        $form = $this->formFactory->createEditForm();
        $form->onSuccess[] = function (Form $form) {
            $this->redirect('Employer:default');
        };
        return $form;
    }

    /**
     * Metoda pro vytvoření formuláře pro mazání
     * @return Form - formulář
     */
    public function createComponentDeleteForm()
    {
        $form = $this->formFactory->createDeleteForm();
        $form->onSuccess[] = function (Form $form) {
            $this->redirect('Employer:default');
        };
        return $form;
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderEdit($id) {
        $emp = $this->_employerModel->getEmployer($id);
        $this->template->name = $emp->firstname . ' ' . $emp->surname;
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderDelete($id) {
        $this->_employerModel->deleteEmployer($id);
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderDefault() {
        $this->template->employers = $this->_employerModel->listEmployers();
    }
}
