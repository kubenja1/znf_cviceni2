<?php

namespace App\Presenters;

use App\Model\CompanyModel;
use App\Model\EmployerModel;
use App\Model\StatisticModel;
use Nette\Application\UI\Presenter;

/**
 * Class BasePresenter
 * @package App\Presenters
 */
abstract class BasePresenter extends Presenter
{
    /** @var  CompanyModel */
    protected $_companyModel;

    /** @var  EmployerModel */
    protected $_employerModel;

    /** @var  StatisticModel */
    protected $_statisticModel;

    public function injectDependencies(
        CompanyModel $companyModel,
        EmployerModel $employerModel,
        StatisticModel $statisticModel)
    {
        $this->_companyModel = $companyModel;
        $this->_employerModel = $employerModel;
        $this->_statisticModel = $statisticModel;
    }
}
