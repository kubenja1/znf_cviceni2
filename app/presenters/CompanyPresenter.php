<?php

namespace App\Presenters;

use App\Model\CompanyModel;
use App\Forms\CompanyFormFactory;
use Nette\Application\UI\Form;
use App\Model\NoDataFound;
use Tracy\Debugger;



class CompanyPresenter extends BasePresenter
{
    /** @var CompanyFormFactory - Formulářová továrnička pro správu firem */
    protected $_companyFormFactory;

    /**
     * Setter pro formulářovou továrničku a model správy firem.
     * @param CompanyFormFactory $companyFormFactory automaticky injectovaná formulářová továrnička pro správu firem
     */
    public function injectCompanyDependencies(CompanyFormFactory $companyFormFactory)
    {
        $this->_companyFormFactory = $companyFormFactory;
    }

    /**
     * Akce pro editaci
     * @param int $id id firmy
     */
    public function actionEdit($id) {
        $form = $this['editForm'];
        $form->setDefaults($this->_companyModel->getCompany($id));
    }

    /**
     * Akce pro mazání
     * @param int $id id firmy
     */
    public function actionDelete($id) {
        $form = $this['deleteForm'];
        $this->_companyModel->deleteCompany($id);
    }

    /**
     * Metoda pro vytvoření formuáře pro vložení
     * @return Form - formulář
     */
    public function createComponentAddForm()
    {
        $form = $this->_companyFormFactory->createAddForm();
        $form->onSuccess[] = function (Form $form) {
            $this->redirect('Company:default');
        };
        return $form;
    }

    /**
     * Metoda pro vytvoření formuáře pro editaci
     * @return Form - formulář
     */
    public function createComponentEditForm()
    {
        $form = $this->_companyFormFactory->createEditForm();
        $form->onSuccess[] = function (Form $form) {
            $this->redirect('Company:default');
        };
        return $form;
    }

    /**
     * Metoda pro vytvoření formuáře pro mazání
     * @return Form - formulář
     */
    public function createComponentDeleteForm()
    {
        $form = $this->_companyFormFactory->createDeleteForm();
        $form->onSuccess[] = function (Form $form) {
            $this->redirect('Company:default');
        };
        return $form;
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderEdit($id) {
        $this->template->name = $this->_companyModel->getCompany($id);
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderDelete($id) {
        $this->_companyModel->deleteCompany($id);
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderDefault() {
        $this->template->companies = $this->_companyModel->listCompanies();
    }
}